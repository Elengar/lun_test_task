import os
import json
from datetime import date

from multiprocessing import Process, Queue


from twisted.internet import reactor
from scrapy.crawler import CrawlerProcess, CrawlerRunner
from scrapy.utils.project import get_project_settings
from scrapy.utils.log import configure_logging

from lun_test_task.spiders import SpiderDobovoLists, SpiderDobovoFlats, SpiderDobovoCalendar
from lun_test_task.spiders import SpiderOktvLists, SpiderOktvFlats, SpiderOktvCalendars


def get_last_result_by_spider_name(spider_name):
    file_name = max(filter(lambda file_name: spider_name in file_name, os.listdir('./results/')))

    items = []

    with open(os.path.join('./results/', file_name), 'r') as f:
        content = f.read()
        if content:
            items = json.loads(content)

    return items


def crawl(spiders):
    def process(q):
        try:
            configure_logging()
            runner = CrawlerRunner(get_project_settings())
            for spider_cls, args, kwargs in spiders:
                runner.crawl(spider_cls, *args, **kwargs)
            d = runner.join()
            d.addBoth(lambda _: reactor.stop())

            reactor.run()  # the script will block here until all crawling jobs are finished

            q.put(None)
        except Exception as e:
            q.put(e)

    error_queue = Queue()
    p = Process(target=process, args=(error_queue,))
    p.start()
    error = error_queue.get()
    p.join()

    if error:
        raise error


def main():
    spiders = [
        (SpiderDobovoLists, [], {}),
        (SpiderOktvLists, [], {}),
    ]

    crawl(spiders)

    dobovo_flat_urls = [result['url'] for result in get_last_result_by_spider_name(SpiderDobovoLists.name)]
    oktv_flat_urls = [result['url'] for result in get_last_result_by_spider_name(SpiderOktvLists.name)]

    spiders = [
        (
            SpiderDobovoFlats,
            [],
            {'start_urls': dobovo_flat_urls}
        ),
        (
            SpiderDobovoCalendar,
            [],
            {'start_urls': dobovo_flat_urls, 'calendar_from_date': date.today(), 'calendar_to_date': date(2018, 1, 1)}
        ),


        (
            SpiderOktvFlats,
            [],
            {'start_urls': oktv_flat_urls}
        ),
        (
            SpiderOktvCalendars,
            [],
            {'start_urls': oktv_flat_urls, 'calendar_from_date': date.today(), 'calendar_to_date': date(2018, 1, 1)}
        ),
    ]

    crawl(spiders)

    results = {spider_cls.name: get_last_result_by_spider_name(spider_cls.name) for spider_cls, args, kwargs in spiders}

    a = 0


if __name__ == "__main__":
    main()
