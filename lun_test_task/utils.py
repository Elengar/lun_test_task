import datetime
import calendar


def months_range(start_year, start_month, end_year, end_month):
    months = []

    curr_year = start_year
    curr_month = start_month

    curr_date = datetime.date(start_year, start_month, 1)
    end_date = datetime.date(end_year, end_month, 1)

    while curr_date <= end_date:
        months.append(curr_date)

        curr_month += 1
        if curr_month > 12:
            curr_year += 1
            curr_month = 1

        curr_date = datetime.date(curr_year, curr_month, 1)

    return months


def add_months(source_date, months):
    month = source_date.month - 1 + months
    year = int(source_date.year + month / 12)
    month = month % 12 + 1
    day = min(source_date.day, calendar.monthrange(year, month)[1])
    return datetime.date(year, month, day)
