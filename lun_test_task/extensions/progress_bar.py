import scrapy
from scrapy.exceptions import NotConfigured
from scrapy import signals

from tqdm import tqdm


class TqdmProgressBar:
    progress_bars = {}
    position = 0

    @classmethod
    def from_crawler(cls, crawler):
        # first check if the extension should be enabled and raise
        # NotConfigured otherwise
        if not crawler.settings.getbool('TQDM_PROGRESS_BARS_ENABLED'):
            raise NotConfigured

        ext = cls()

        # connect the extension object to signals
        crawler.signals.connect(ext.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(ext.spider_closed, signal=signals.spider_closed)
        crawler.signals.connect(ext.item_scraped, signal=signals.item_scraped)
        crawler.signals.connect(ext.engine_stopped, signal=signals.engine_stopped)

        return ext

    def spider_opened(self, spider):
        if hasattr(spider, 'total_items_amount') and spider.name not in self.progress_bars:
            self.progress_bars[spider.name] = tqdm(
                total=spider.total_items_amount, desc=spider.name, leave=True, position=type(self).position
            )

            type(self).position += 1

    def spider_closed(self, spider):
        if spider.name in self.progress_bars:
            self.progress_bars[spider.name].close()

    def item_scraped(self, item, spider):
        if spider.name in self.progress_bars:
            progress_bar = self.progress_bars[spider.name]
            if spider.total_items_amount != progress_bar.total:
                progress_bar.total = spider.total_items_amount

            progress_bar.update(1)
            progress_bar.set_description(
                '{} | queue {}'.format(spider.name, len(spider.crawler.engine.slot.scheduler))
            )

    def engine_stopped(self, sender=None):
        spider_name = sender.spider.name

        bar_position = self.progress_bars[spider_name].pos
        min_position = min(bar.pos for bar in self.progress_bars.values())
        if bar_position == min_position:
            print()

            for bar in self.progress_bars.values():
                bar.pos -= 1

            type(self).position -= 1

        if len(self.progress_bars) == 1:
            print('\n' * type(self).position, end='')
            type(self).position = 0

        del self.progress_bars[spider_name]
