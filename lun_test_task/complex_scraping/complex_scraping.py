import scrapy


class _ComplexRequestMixin:
    def __init__(self, *args, **kwargs):
        item = None
        if 'meta' in kwargs and 'item' in kwargs['meta']:
            item = kwargs['meta']['item']

        if not isinstance(item, ComplexItem):
            raise ValueError(f'Expected ComplexItem in meta["item"], got {type(item)}')

        kwargs['callback'] = self.callbacks_decorator(kwargs['callback'])

        super().__init__(*args, **kwargs)

        item.special_fields['request_pool'].append(self)

    @staticmethod
    def callbacks_decorator(callback):
        def decorated(response):
            item = response.meta['item']
            request = response.request
            request_pool = item.special_fields['request_pool']

            if request not in request_pool:
                # If request was redirected, response will call callback twice
                # I didn't figure out what is the point of such behaviour, but there is a dirty hack below
                unbounded_request = True

                if 'redirect_urls' in request.meta:
                    redirected_urls = request.meta['redirect_urls']
                    for redirected_url in redirected_urls:
                        for waited_request in request_pool:
                            if waited_request.url == redirected_url:
                                request_pool.remove(waited_request)
                                unbounded_request = False

                if unbounded_request:
                    raise RuntimeError(f"Unbounded request {request} for item {item}")

            result = callback(response)
            request_pool.remove(request)

            # If this is the last request
            if not result and len(request_pool) == 0:
                return item
            else:
                return result

        return decorated


class ComplexRequest(_ComplexRequestMixin, scrapy.Request):
    pass


class ComplexFormRequest(_ComplexRequestMixin, scrapy.FormRequest):
    pass


class ComplexItem(scrapy.Item):
    def __init__(self, *args, **kwargs):
        self.special_fields = {}
        self.special_fields['request_pool'] = []

        super().__init__(*args, **kwargs)

    def __getattr__(self, name):
        if name == "special_fields":
            object.__getattribute__(self, name)
        else:
            return super().__getattr__(name)

    def __setattr__(self, name, value):
        if name == "special_fields":
            object.__setattr__(self, name, value)
        else:
            super().__setattr__(name, value)
