import json


import scrapy
from scrapy.spiders import Rule
from scrapy.linkextractors import LinkExtractor


from lun_test_task.spiders.AbstractFlatSpider import AbstractFlatSpider, CalendarScraperMixin, TqdmScraperMixin
from lun_test_task.complex_scraping import ComplexFormRequest

from lun_test_task.items import FlatBaseItem, FlatCalendarItem, FlatInfoItem


class DobovoAbscractSpider(AbstractFlatSpider, TqdmScraperMixin):
    allowed_domains = ['dobovo.com']

    @staticmethod
    def get_site_id_from_url(url):
        return url.split('.')[-2].split('-')[-1]

    @staticmethod
    def get_item_id(site_id):
        return f"dobovo_{site_id}"


class SpiderDobovoLists(DobovoAbscractSpider):
    name = "www.dobovo.com lists"

    start_urls = ['https://www.dobovo.com/kiev-apartments.html']
    rules = (
        Rule(
            LinkExtractor(restrict_css=('.pages a.selected ~ a',), unique=True),
            callback='parse_flats_list', follow=True
        ),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.total_items_amount = None

    def parse_start_url(self, response):
        flats_amount = int(response.css('#apt_total_counter ::text').extract_first().split()[0])
        self.total_items_amount = flats_amount

        return self.parse_flats_list(response)

    def parse_flats_list(self, response):
        for html_flat_item in response.css('div.item.dbv_js_aptblock'):
            url = html_flat_item.css('::attr(data-apt-url)').extract_first()

            yield self.build_item(FlatBaseItem, url)


class SpiderDobovoFlats(DobovoAbscractSpider):
    name = "www.dobovo.com flats"

    start_urls = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def parse_start_url(self, response):
        return self.parse_html_flat_page(response)

    def parse_html_flat_page(self, response):
        item = self.build_item(FlatInfoItem, response.url)

        item['title'] = response.css('#dbv_js_title ::text').extract_first().strip()
        item['desc'] = response.css('p[itemprop="description"] ::text').extract_first().strip('"')

        return item


class SpiderDobovoCalendar(CalendarScraperMixin, DobovoAbscractSpider):
    name = "www.dobovo.com calendars"

    start_urls = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.prepared_months = list(map(str, self.months))
        # api method returns requested month + next month, so we can skip every second
        self.prepared_months = self.prepared_months[::2]

    def start_requests(self):
        for url in self.start_urls:
            item = self.build_item(FlatCalendarItem, url)

            for month in self.prepared_months:
                yield ComplexFormRequest(
                    'https://www.dobovo.com/dobovo/apt/ajax.php?action=getCalendar&lang=en',
                    method='POST',
                    formdata={
                        'id': item['site_id'],
                        'date': month,
                    },
                    meta={
                        'item': item,
                    },
                    callback=self.parse_api_calendar,
                    dont_filter=True,
                )

    def parse_api_calendar(self, response):
        item = response.meta['item']

        data = json.loads(response.text)

        for day_date, day_value in data['calendar'].items():
            if day_value['currency'] != 'UAH':
                raise ValueError(f'Currency {day_value["currency"]} needs to be converted. Day {day_value!r}')

            item['calendar'].update(
                {day_date: {'price': day_value['price'], 'is_available': bool(int(day_value['state']))}}
            )

        for discount in data['discounts']:
            item['wholesales_calendar'].update(
                {int(discount['days']): {'discount_percent': float((discount['discount']))}}
            )
