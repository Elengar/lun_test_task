from datetime import datetime
import json
import re


import scrapy
from scrapy.spiders import Rule
from scrapy.linkextractors import LinkExtractor


from lun_test_task.utils import add_months

from lun_test_task.spiders.AbstractFlatSpider import AbstractFlatSpider, CalendarScraperMixin, TqdmScraperMixin
from lun_test_task.complex_scraping import ComplexFormRequest

from lun_test_task.items import FlatBaseItem, FlatInfoItem, FlatCalendarItem


class OktvAbscractSpider(AbstractFlatSpider, TqdmScraperMixin):
    allowed_domains = ['oktv.ua']
    url_id_regex = re.compile('/id(\d+)')

    @staticmethod
    def get_site_id_from_url(url):
        return OktvAbscractSpider.url_id_regex.search(url).group(1)

    @staticmethod
    def get_item_id(site_id):
        return f"oktv_{site_id}"


class SpiderOktvLists(OktvAbscractSpider):
    name = "oktv.ua lists"

    start_urls = ['https://oktv.ua/kiev/arenda-kvartir-posutochno']
    rules = (
        Rule(
            LinkExtractor(restrict_css=('#searchPagination div ul.pagination li a',), unique=True),
            callback='parse_flats_list', follow=True
        ),
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.total_items_amount = None

    def parse_start_url(self, response):
        flats_amount = int(response.css('#searchPagination ul.pagination li:last-child i ::text').extract_first())
        self.total_items_amount = flats_amount

        # don't parse first page because this page will be extracted using LinkExtractor
        # return self.parse_flats_list(response)

    def parse_flats_list(self, response):
        for html_flat_item in response.css('#apTilesContainer div.object_v_spiske div:first-child a:first-child'):
            relative_url = html_flat_item.css('::attr(href)').extract_first()
            url = response.urljoin(relative_url)

            yield self.build_item(FlatBaseItem, url)


class SpiderOktvFlats(OktvAbscractSpider):
    name = "oktv.ua flats"

    start_urls = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def parse_start_url(self, response):
        return self.parse_html_flat_page(response)

    def parse_html_flat_page(self, response):
        item = self.build_item(FlatInfoItem, response.url)

        item['title'] = response.css('.h1-apart ::text').extract_first().strip()
        item['desc'] = (response.css('div.object_desc > div.desc > div ::text').extract_first() or '').strip()

        return item


class SpiderOktvCalendars(CalendarScraperMixin, OktvAbscractSpider):
    name = "oktv.ua calendars"

    start_urls = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.prepared_months = [
            (int(datetime.combine(month, datetime.min.time()).timestamp()), month)
            for month in self.months
        ]
        # api method returns requested month + next month, so we can skip every second
        # but we need not to skip last month because of calendars structure on oktv
        # (it is possible that last days of current month will be in second month data)
        self.prepared_months = self.prepared_months[:-1:2] + [self.prepared_months[-1]]

    def start_requests(self):
        for url in self.start_urls:
            item = self.build_item(FlatCalendarItem, url)

            for month_timestamp, month_date in self.prepared_months:
                yield ComplexFormRequest(
                    'http://oktv.ua/id{}'.format(item['site_id']),  # DOESN'T WORK VIA HTTPS
                    method='POST',
                    formdata={
                        'paiAction': 'app/ajax/publicCommonAjax',
                        'stringdata': 'publicAction => calendar; id => {id}; point => {point_timestamp}'.format(
                            id=item['site_id'],
                            point_timestamp=month_timestamp
                        ),
                    },
                    headers={
                        'X-Requested-With': 'XMLHttpRequest',
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                    },
                    meta={
                        'item': item,
                        'month_date': month_date,
                    },
                    callback=self.parse_calendar,
                    dont_filter=True,
                )

    def parse_calendar(self, response):
        item = response.meta['item']

        html = json.loads(response.text)['html']['apCalendarZone']
        selector = scrapy.Selector(text=html)

        first_calendar, second_calendar = selector.css('div.cal-row')
        first_month_date = response.meta['month_date']
        second_month_date = add_months(first_month_date, 1)

        for calendar, month_date in zip([first_calendar, second_calendar], [first_month_date, second_month_date]):
            result = {}

            # oktv can return some days from the previous month in the beginning of current month data
            month_date = add_months(month_date, -1)

            for day in calendar.css('.day'):
                number = int(day.css('.num ::text').extract_first())

                # oktv can return 31 day for every month
                try:
                    day_date = month_date.replace(day=number)
                except ValueError:
                    continue

                price = day.css('.price ::text').extract_first().strip()
                is_available = not bool(day.css('[class*="bron"]'))
                if 'грн' in price:
                    price = int(price.strip('грн'))
                else:
                    raise ValueError(f'Currency {price} needs to be converted. Day {number!r}')

                # the beginning of next month
                if number == 1:
                    month_date = add_months(month_date, 1)

                result[str(day_date)] = {"price": price, "is_available": is_available}

            item['calendar'].update(result)
