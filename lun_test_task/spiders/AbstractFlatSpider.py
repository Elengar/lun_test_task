from datetime import date


from scrapy.spiders import CrawlSpider


from lun_test_task.utils import months_range


class TqdmScraperMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if hasattr(self, 'start_urls'):
            self.total_items_amount = len(self.start_urls)
        else:
            self.total_items_amount = None


class AbstractFlatSpider(TqdmScraperMixin, CrawlSpider):
    def build_item(self, item_cls, url):
        item = item_cls()

        item['url'] = url
        item['site_id'] = self.get_site_id_from_url(item['url'])
        item['item_id'] = self.get_item_id(item['site_id'])

        return item

    @staticmethod
    def get_site_id_from_url(url):
        raise NotImplementedError

    @staticmethod
    def get_item_id(site_id):
        raise NotImplementedError


class CalendarScraperMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.calendar_from_date = hasattr(self, 'calendar_from_date') and self.calendar_from_date or date.today()
        self.calendar_to_date = hasattr(self, 'calendar_to_date') and self.calendar_to_date or date.today()

        self.months = months_range(
            *(self.calendar_from_date.timetuple()[:2]),
            *(self.calendar_to_date.timetuple()[:2]),
        )
        self.prepared_months = self.months
