# -*- coding: utf-8 -*-


import scrapy
from lun_test_task.complex_scraping import ComplexItem


class FlatBaseItem(ComplexItem):
    url = scrapy.Field()

    # item_id - unique identifier of this flat
    item_id = scrapy.Field()

    # site_id - identifier that used on this flat's site
    site_id = scrapy.Field()


class FlatInfoItem(FlatBaseItem):
    title = scrapy.Field()
    desc = scrapy.Field()
    # photos = scrapy.Field()


class FlatCalendarItem(FlatBaseItem):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self['calendar'] = {}
        self['wholesales_calendar'] = {}

    calendar = scrapy.Field()
    wholesales_calendar = scrapy.Field()
