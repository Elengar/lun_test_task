DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

VENV_DIRECTORY_NAME="venv"
VENV_DIRECTORY_PATH="$DIR/$VENV_DIRECTORY_NAME"

https_proxy='82.117.232.13:3128'

# Activate venv
source "$VENV_DIRECTORY_PATH/bin/activate"

# export https_proxy

python main.py "$@"